
itweak_upload 6.x-1.x-dev
-------------------------


itweak_upload 6.x-1.2, 2010-02-17
---------------------------------
[#711134] by maximpodorov: Fix for PHP 5.3 "Parameter 1 to itweak_upload_upload_form_current() expected to be a reference, value given"
SA-CONTRIB-2010-017 by iva2k: remove XSS vulnerability in file names

itweak_upload 6.x-1.1, 2009-07-29
---------------------------------
[NOISSUE] by iva2k: Added margin-right to attachments div in itweak_upload.css (for cleaner advanced forum styling).
[#520286] by jackinloadup: Fixed .eps icons not showing up.
[NOISSUE] by iva2k: Added missing psd.png for mime-16.

itweak_upload 6.x-1.0, 2009-07-03
---------------------------------
[NOISSUE] by iva2k: Initial commit, First Release.
[NOISSUE] by iva2k: Initial commit, First Release - apply theme tweaks to comment attachments.
